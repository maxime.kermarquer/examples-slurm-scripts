#!/bin/bash
#SBATCH --job-name=hello-world-matlab
#SBATCH --partition=normal,bigmem
#SBATCH --mem=1G
#SBATCH --cpus-per-task=1
#SBATCH --chdir=/network/lustre/iss01/home/maxime.kermarquer/examples-slurm-scripts/basics_examples/MATLAB
#SBATCH --output=outputs/hello-world-matlab-%j.txt
#SBATCH --error=outputs/hello-world-matlab-%j.txt

module load MATLAB
matlab -nodesktop -softwareopengl -nosplash -nodisplay < matlab-scripts/hello-world.m
