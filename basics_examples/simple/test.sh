#!/bin/bash
#SBATCH --partition=bigmem                                # type of server
#SBATCH --time=01:00:00                                   # time limit for the job
#SBATCH --mem=4G                                          # memory for the job
#SBATCH --cpus-per-task=1                                 # number of processors for the job
#SBATCH --job-name="test"                                 # job name
#SBATCH --chdir=.                                         # working directory
#SBATCH --output=test-out-%N-%j.txt                       # standart output
#SBATCH --error=test-err-%N-%j.txt                        # standard error
#SBATCH --mail-user=maxime.kermarquer@icm-institute.org   # your mail
#SBATCH --mail-type=ALL                                   # type of notifications you want to receive



hostname
lscpu
sleep 10
echo "Write your commands"
