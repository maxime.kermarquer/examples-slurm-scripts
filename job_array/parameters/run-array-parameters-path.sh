#!/bin/bash
#SBATCH --partition=bigmem,normal
#SBATCH --time=02:00:00
#SBATCH --mem=2G
#SBATCH --cpus-per-task=1
#SBATCH --chdir=.
#SBATCH --error=outputs/job_%A_%a.log
#SBATCH --output=outputs/job_%A_%a.log
#SBATCH --array=1-10

module load MATLAB


export PARAMETER=$(sed -n ${SLURM_ARRAY_TASK_ID}p parameters/parameters-path.txt)


if [ ! -z $PARAMETER ]; then
    echo "Use parameter : ${PARAMETER}"
    matlab -nojvm -nodisplay -nodesktop < matlab-scripts/test_array.m
fi
