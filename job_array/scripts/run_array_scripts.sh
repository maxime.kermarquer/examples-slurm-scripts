#!/bin/bash
#SBATCH --partition=bigmem
#SBATCH --time=02:00:00
#SBATCH --mem=2G
#SBATCH --cpus-per-task=1
#SBATCH --error=outputs/job_%N_%A_%a.log
#SBATCH --output=outputs/job_%N_%A_%a.log
#SBATCH --chdir=.
#SBATCH --array=1-3

echo "SLURM_JOBID: " $SLURM_JOBID
echo "SLURM_ARRAY_JOB_ID: " $SLURM_ARRAY_JOB_ID
echo "SLURM_ARRAY_TASK_ID: " $SLURM_ARRAY_TASK_ID

sleep 5
module load python/2.7
python scripts/script_${SLURM_ARRAY_TASK_ID}.py
