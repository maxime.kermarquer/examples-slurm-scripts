#!/bin/bash

#SBATCH --partition=normal
#SBATCH --cpus-per-task=1
#SBATCH --mem=4G
#SBATCH --time=01:00:00
#SBATCH --chdir=.
#SBATCH --output=outputs/output_R_GMMAT.txt
#SBATCH --error=outputs/output_R_GMMAT.txt
#SBATCH --job-name="run_GMMAT"

module load R/3.5.0
R CMD BATCH example_gmmat.R
