#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --job-name="dependency_gpu"
#SBATCH --mem=4MB
#SBATCH --partition=gpu
#SBATCH --gres=gpu:1
#SBATCH --chdir=.
#SBATCH --output=outputs/gpu_%j.log
#SBATCH --error=outputs/gpu_%j.log

hostname
echo "I am computing on GPU..."
sleep 15
