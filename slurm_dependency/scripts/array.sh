#!/bin/bash
#SBATCH --partition=normal
#SBATCH --job-name="dependency_array"
#SBATCH --mem=4M
#SBATCH --ntasks=1
#SBATCH --chdir=.
#SBATCH --output=outputs/array_%A_%a.log
#SBATCH --error=outputs/array_%A_%a.log
#SBATCH --array=1-5

hostname
echo Hello World $SLURM_ARRAY_TASK_ID !
sleep $(( ( RANDOM % 60 )  + 1 ))
