#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --job-name="dependency_concatenate"
#SBATCH --mem=4MB
#SBATCH --partition=bigmem
#SBATCH --chdir=.
#SBATCH --output=outputs/concatenate_%j.log
#SBATCH --error=outputs/concatenate_%j.log

hostname
echo "I am concatenating..."
sleep 15
