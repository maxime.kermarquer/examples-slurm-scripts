#!/bin/bash

JOBID=$(sbatch scripts/array.sh | awk '{print $4}')
JOBID=$(sbatch --dependency afterok:$JOBID scripts/concatenate.sh | awk '{print $4}')
sbatch --dependency afterok:$JOBID scripts/gpu.sh
