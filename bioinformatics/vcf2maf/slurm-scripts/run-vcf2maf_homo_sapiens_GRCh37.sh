#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=1
#SBATCH --mem=4G
#SBATCH --time=01:00:00
#SBATCH --chdir=.
#SBATCH --output=outputs/vcf2maf_homo_sapiens_GRCh37-%j.txt
#SBATCH --error=outputs/vcf2maf_homo_sapiens_GRCh37-%j.txt
#SBATCH --job-name=vcf2maf_homo_sapiens_GRCh37

echo -e "--- \e[34mModule loading\e[0m ---"
module load vcf2maf

echo -e "--- \e[34mStart\e[0m ---"

export INPUT_VCF=data/homo_sapiens_GRCh37.vcf
export OUTPUT_MAF=results/homo_sapiens_GRCh37.vep.maf
export FASTA_FILE=${VEP_DATA}/homo_sapiens/95_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa
export FILTER_VCF=$VEP_DATA/ExAC_nonTCGA.r0.3.1.sites.vep.vcf.gz

echo -e "\e[33mperl ${VCF2MAF_PATH}/vcf2maf.pl --vep-path ${VEP_PATH} --vep-data ${VEP_DATA} --input-vcf ${INPUT_VCF} --output-maf ${OUTPUT_MAF} --ref-fasta ${FASTA_FILE} --filter-vcf ${FILTER_VCF}\e[0m"
perl ${VCF2MAF_PATH}/vcf2maf.pl --vep-path ${VEP_PATH} --vep-data ${VEP_DATA} --input-vcf ${INPUT_VCF} --output-maf ${OUTPUT_MAF} --ref-fasta ${FASTA_FILE} --filter-vcf ${FILTER_VCF}

echo -e "--- \e[34mEnd\e[0m ---"
