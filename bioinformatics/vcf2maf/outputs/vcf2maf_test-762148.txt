--- [34mModule loading[0m ---
--- [34mStart[0m ---
[33mperl /network/lustre/iss01/apps/software/scit/vcf2maf/1.6.17/maf2maf.pl --vep-path /network/lustre/iss01/apps/software/scit/ensembl-vep/release-95.3 --vep-data /network/lustre/iss01/apps/software/scit/ensembl-vep/references --input-maf data/test.maf --output-maf results/test_reannotate.vep.maf --ref-fasta /network/lustre/iss01/apps/software/scit/ensembl-vep/references/homo_sapiens/95_GRCh37/Homo_sapiens.GRCh37.75.dna.primary_assembly.fa --filter-vcf /network/lustre/iss01/apps/software/scit/ensembl-vep/references/ExAC_nonTCGA.r0.3.1.sites.vep.vcf.gz[0m
WARNING: Replacing '-' with reference allele in: PIK3CA	0	.	GRCh37	3	178936091	178936091	+	Missense_Mutation	SNP	G	-	A			TUMOR	NORMAL	G	G																43	21	22	11	11	0
STATUS: Running VEP and writing to: /tmp/VBe4cNyfD3/test.vep.vcf
Could not load .tbi/.csi index of /network/lustre/iss01/apps/software/scit/ensembl-vep/references/ExAC_nonTCGA.r0.3.1.sites.vep.vcf.gz
--- [34mEnd[0m ---
