#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=16
#SBATCH --mem=32G
#SBATCH --time=01:00:00
#SBATCH --chdir=.
#SBATCH --output=outputs/OptiType-DNA-%j.txt
#SBATCH --error=outputs/OptiType-DNA-%j.txt
#SBATCH --job-name=OptiType-DNA

echo -e "--- \e[1;34mModule loading\e[0m ---"
module load optitype/1.3.5


echo -e "--- \e[1;34mCompute node\e[0m ---"
hostname

echo -e "--- \e[1;34mStart\e[0m ---"
python src/OptiTypePipeline.py --config config/config.ini --input data/exome/NA11995_SRR766010_1_fished.fastq data/exome/NA11995_SRR766010_2_fished.fastq --dna --verbose --outdir results/exome
echo -e "--- \e[1;34mEnd\e[0m ---"
