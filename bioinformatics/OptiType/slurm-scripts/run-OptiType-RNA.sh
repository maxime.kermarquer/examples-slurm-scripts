#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=16
#SBATCH --mem=32G
#SBATCH --time=01:00:00
#SBATCH --chdir=.
#SBATCH --output=outputs/OptiType-RNA-%j.txt
#SBATCH --error=outputs/OptiType-RNA-%j.txt
#SBATCH --job-name=OptiType-RNA

echo -e "--- \e[1;34mModule loading\e[0m ---"
module load optitype/1.3.5

echo -e "--- \e[1;34mCompute node\e[0m ---"
hostname

echo -e "--- \e[1;34mStart\e[0m ---"
python src/OptiTypePipeline.py --config config/config.ini --input data/rna/CRC_81_N_1_fished.fastq data/rna/CRC_81_N_2_fished.fastq --rna --verbose --outdir results/rna
echo -e "--- \e[1;34mEnd\e[0m ---"
