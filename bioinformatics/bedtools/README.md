# bedtools

This example comes from the bedtools tutorial 

http://quinlanlab.org/tutorials/bedtools/bedtools.html

## Run the bedtools example job

```bash
sbatch slurm-scripts/run-bedtools.sh 
``` 
