#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=1
#SBATCH --mem=4G
#SBATCH --time=01:00:00
#SBATCH --chdir=.
#SBATCH --output=outputs/bedtools-%j.txt
#SBATCH --error=outputs/bedtools-%j.txt
#SBATCH --job-name=bedtools

echo -e "--- \e[1;34mModule loading\e[0m ---"
module load bedtools

echo -e "--- \e[1;34mStart\e[0m ---"
tar -xvf data/maurano.dnaseI.tgz -C data
bedtools intersect -a data/cpg.bed -b data/exons.bed -wa -wb
echo -e "--- \e[1;34mEnd\e[0m ---"
