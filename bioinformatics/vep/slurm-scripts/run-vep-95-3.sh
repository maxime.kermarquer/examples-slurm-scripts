#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=1
#SBATCH --mem=4G
#SBATCH --time=01:00:00
#SBATCH --chdir=.
#SBATCH --output=outputs/run-vep-95-3-%j.txt
#SBATCH --error=outputs/run-vep-95-3-%j.txt
#SBATCH --job-name=run-vep-95-3

echo -e "--- \e[34mModule loading\e[0m ---"
module load vep/95.3

echo -e "--- \e[34mStart\e[0m ---"
echo -e "\e[33mvep --input_file $VEP_PATH/examples/homo_sapiens_GRCh38.vcf --output_file results/variant_effect_output.txt --offline  --dir_cache $VEP_DATA --assembly GRCh38\e[0m"

vep --input_file $VEP_PATH/examples/homo_sapiens_GRCh38.vcf --output_file results/variant_effect_output.txt --offline  --dir_cache $VEP_DATA --assembly GRCh38

echo -e "--- \e[34mEnd\e[0m ---"
