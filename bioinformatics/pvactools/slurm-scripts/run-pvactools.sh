#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=2
#SBATCH --mem=8G
#SBATCH --time=01:00:00
#SBATCH --chdir=.
#SBATCH --output=outputs/pvactools-%j.txt
#SBATCH --error=outputs/pvactools-%j.txt
#SBATCH --job-name=pvactools

echo -e "--- \e[1;34mModule loading\e[0m ---"
module load pvactools/2.0.4
module load proxy # To allow internet access to fetch sample data
module list

echo -e "--- \e[1;34mStart\e[0m ---"
mhcflurry-downloads fetch models_class1_presentation
pvacseq run data/pvacseq_example_data/input.vcf Test "HLA-A*02:01,HLA-B*35:01,DRB1*11:01" MHCflurry MHCnuggetsI MHCnuggetsII NNalign NetMHC PickPocket SMM SMMPMBEC SMMalign results -e1 8,9,10 -e2 15
echo -e "--- \e[1;34mEnd\e[0m ---"
