#!/bin/bash
#SBATCH --partition=normal
#SBATCH --time=10:00:00
#SBATCH --mem=16G
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --chdir=.
#SBATCH --output=outputs/test_FSL_%j.out
#SBATCH --error=outputs/test_FSL_%j.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=maxime.kermarquer@icm-institute.org
#SBATCH --job-name=test_FSL

module load FSL

flirt --help;
fnirt --help;

#flirt -in <arg> -ref <arg> -out <arg> -omat <arg>
#fnirt  --ref= <arg> --in= <arg> --iout= <arg> --aff= <arg> --cout= <arg> --fout= <arg> --jout= <arg>

exit 0
