#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=8
#SBATCH --mem=32G
#SBATCH --time=20:00:00
#SBATCH --chdir=.
#SBATCH --output=outputs-slurm/fmriprep-20.0.0-test-%j.txt
#SBATCH --error=outputs-slurm/fmriprep-20.0.0-test-%j.txt
#SBATCH --job-name=fmriprep-20.0.0-test

echo -e "--- \e[34mModule loading\e[0m ---"
echo -e "\e[33mmodule load fmriprep/20.0.0\e[0m"
module load fmriprep/20.0.0

echo -e "\e[33mmodule list\e[0m"
module list



echo -e "--- \e[34mfmriprep arguments\e[0m ---"
export BIDS_DIR=$(realpath ../bids-example/ds001_BIDS)
export OUTPUT_DIR=$(realpath outputs)
export PARTICIPANT=participant
export WORK_DIRECTORY=$(realpath workdir)
export OPTIONS="--verbose --work-dir ${WORK_DIRECTORY}  --nthreads ${SLURM_CPUS_PER_TASK} --mem_mb ${SLURM_MEM_PER_NODE}"

echo -e "\e[35m
FMRIPREP: fMRI PREProcessing workflows

positional arguments:
  bids_dir              the root folder of a BIDS valid dataset (sub-XXXXX
                        folders should be found at the top level in this
                        folder).
  output_dir            the output path for the outcomes of preprocessing and
                        visual reports
  {participant}         processing stage to be run, only "participant" in the
                        case of FMRIPREP (see BIDS-Apps specification).
\e[0m"

echo -e "\e[33mbids_dir=\e[0m${BIDS_DIR}"
echo -e "\e[33moutput_dir=\e[0m${OUTPUT_DIR}"
echo -e "\e[33m{participant}=\e[0m${PARTICIPANT}"
echo -e "\e[33mOptions=\e[0m${OPTIONS}"

echo -e ""

echo -e "--- \e[34mStart\e[0m ---"
echo -e "\e[33mfmriprep --verbose ${BIDS_DIR} ${OUTPUT_DIR} ${PARTICIPANT} --work-dir ${WORK_DIRECTORY} ${OPTIONS}\e[0m"
fmriprep ${BIDS_DIR} ${OUTPUT_DIR} ${PARTICIPANT} --work-dir ${WORK_DIRECTORY} ${OPTIONS}
echo -e "--- \e[34mEnd\e[0m ---"
