#!/bin/bash
#SBATCH --partition=gpu
#SBATCH --time=02:00:00
#SBATCH --mem=20G
#SBATCH --cpus-per-task=1
#SBATCH --chdir=.
#SBATCH --output=outputs/tensorflow_gpu_job_%j.log
#SBATCH --error=outputs/tensorflow_gpu_job_%j.log
#SBATCH --mail-type=ALL
#SBATCH --mail-user=maxime.kermarquer@icm-institute.org
#SBATCH --job-name=test_tensorflow
#SBATCH --gres=gpu:1

module load tensorflow/1.15.2

python tensorflow_example.py
