#!/bin/bash
#SBATCH --partition=gpu
#SBATCH --time=02:00:00
#SBATCH --mem=20G
#SBATCH --cpus-per-task=1
#SBATCH --gres=gpu:1
#SBATCH --chdir=.
#SBATCH --output=outputs/pytorch_job_%j.log
#SBATCH --error=outputs/pytorch_job_%j.log
#SBATCH --job-name=test_pytorch

module load proxy  python/3.6
python python-scripts/mnist_example.py
