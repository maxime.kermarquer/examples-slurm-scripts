#!/bin/bash
#SBATCH --partition=gpu
#SBATCH --cpus-per-task=1
#SBATCH --mem=4G
#SBATCH --gres=gpu:1
#SBATCH --time=01:00:00
#SBATCH --chdir=.
#SBATCH --output=outputs/pytorch-1.5.0-%j.txt
#SBATCH --error=outputs/pytorch-1.5.0-%j.txt
#SBATCH --job-name=pytorch-1.5.0-test

echo -e "--- \e[1;34mModule loading\e[0m ---"
module load pytorch/1.5.0 proxy

echo -e "--- \e[1;34mStart\e[0m ---"
python python-scripts/torch-geometric-test.py
echo -e "--- \e[1;34mEnd\e[0m ---"
