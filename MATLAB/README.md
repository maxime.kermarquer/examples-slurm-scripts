
* To test a _hello world_ MATLAB script on the cluster :
    * `sbatch slurm-scripts/run-matlab-hello-world.sh`

* To test to run a parallel MATLAB script :
    * Import Parallel Cluster ICM profile :
        * `sbatch slurm-scripts/run-matlab-import-cluster-profiles.sh`
    * To run sequential and parallel MATLAB example :
        *  `sbatch slurm-scripts/run-matlab-parfor.sh`
