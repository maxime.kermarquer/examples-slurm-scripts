#!/bin/bash
#SBATCH --job-name=hello-world-matlab
#SBATCH --partition=normal
#SBATCH --mem=16G
#SBATCH --cpus-per-task=4
#SBATCH --chdir=.
#SBATCH --output=outputs/hello-world-matlab-%j.txt
#SBATCH --error=outputs/hello-world-matlab-%j.txt

module load MATLAB
matlab -nodesktop -softwareopengl -nosplash -nodisplay < matlab-scripts/hello-world.m
sleep 5;
