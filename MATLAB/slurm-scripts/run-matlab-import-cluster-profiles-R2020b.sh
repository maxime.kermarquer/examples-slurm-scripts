#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=8
#SBATCH --mem=16G
#SBATCH --time=01:00:00
#SBATCH --job-name=import-icm-cluster-profile-R2020b
#SBATCH --chdir=.
#SBATCH --output=outputs/import-icm-cluster-profile-R2020b_%j.txt
#SBATCH --error=outputs/import-icm-cluster-profile-R2020b_%j.txt

module load MATLAB/R2020b
matlab -nodesktop -softwareopengl -nosplash -nodisplay < matlab-scripts/import-icm-cluster-profile.m
