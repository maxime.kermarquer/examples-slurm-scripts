#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=8
#SBATCH --mem=16G
#SBATCH --time=01:00:00
#SBATCH --job-name=test-matlab-parfor-R2017b
#SBATCH --chdir=.
#SBATCH --output=outputs/test-matlab-parfor-R2017b-%j.txt
#SBATCH --error=outputs/test-matlab-parfor-R2017b-%j.txt

module load MATLAB/R2017b

echo -e "\e[31m--- Without parfor ---\e[0m"

matlab -nodesktop -noopengl -nodisplay < matlab-scripts/test-for.m

echo -e "\n\e[32m--- With parfor ---\e[0m"

matlab -nodesktop -noopengl -nodisplay < matlab-scripts/test-parfor.m

