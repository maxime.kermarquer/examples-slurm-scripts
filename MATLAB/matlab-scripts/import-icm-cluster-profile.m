%% Import ICM cluster profile

% Check if ICM Cluster profile is already imported.
alreadyImported = false;
allProfiles = parallel.clusterProfiles(); % Get Cluster profiles

for i=1:length(allProfiles)
    if ( strcmp(allProfiles{i}, 'cluster-slurm-icm') )
        fprintf('ICM Cluster profile already imported.\n');
        alreadyImported = true;
        break;
    end
end


% Import ICM cluster profile
if ( ~alreadyImported )
	fprintf('Import ICM cluster profile ...\n');
    profile_icm = parallel.importProfile('cluster-profiles/cluster-slurm-icm.settings');
    parallel.defaultClusterProfile(profile_icm);
end

