#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=1
#SBATCH --mem=4G
#SBATCH --time=01:00:00
#SBATCH --chdir=.
#SBATCH --output=outputs/BCT-demo-%j.txt
#SBATCH --error=outputs/BCT-demo-%j.txt
#SBATCH --job-name=BCT-demo


echo -e "--- \e[1;34mModule loading\e[0m ---"
module load BCT

echo -e "--- \e[1;34mStart\e[0m ---"
echo -e "Command : \e[33mmatlab -nodesktop -softwareopengl -nosplash -nodisplay < matlab-scripts/demo_generative_models_geometric.m\e[0m"
matlab -nodesktop -softwareopengl -nosplash -nodisplay < matlab-scripts/demo_generative_models_geometric.m
echo -e "--- \e[1;34mEnd\e[0m ---"
